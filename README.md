# ErikDevelop Frontend

This is the frontend for my personal page.

## Table of contents
- [Table of contents](#table-of-contents)
- [Project structure](#project-structure)
- [First time setup](#first-time-setup)
- [Running the project](#running-the-project)
- [Perfromance testing](#perfromance-testing)
- [Checking code style](#checking-code-style)
- [Contributing](#contributing)

## Project structure
The project has the following folder structure:
- **erikdevelop-frontend** This is the folder where the vue project is located.
  * **dist** contains a distributable version of the application
  * **public** this contains all the public files like the favicon, the index file, this is also where you put any resources the application might want to use.
  * **src** this folder contains all the vue source code.

## First time setup
When running the project for the first time, cd into `erikdevelop-frontend`. Once there, you should run `npm install` Or you just use this command in one go from the root folder:
```
cd erikdevelop-frontend && npm install && cd ../
```

## Running the project.
There is no docker or any such thing, as the application is supposed to run serverless, just the service will do.
As this script does not run detached, remember to run a separate tab. cd into `erikdevelop-frontend` and run `npm run serve`
```
cd erikdevelop-frontend && npm run serve
```

## Perfromance testing.
When trying to test the page for speed, remember to test this with the built version. The built version uses the minified code. You can do this by cding into `erikdevelop-frontend` and running `npm run build` or the following command:
```
cd erikdevelop-frontend && npm run build && cd ../
```
once you have built the Application, you have to serve it. You can do this by cding into `erikdevelop-frontend` and running `serve` through `npx`. Remember this does not run detached, so open a separate console.
```
cd erikdevelop-frontend/dist && npx serve
```
Don't forget to use chrome to simulate network speeds!

## Checking code style
You can check the code style by cding into `erikdevelop-frontend` and running `npm run lint`
TODO: Create git hook that checks lint.
```
cd erikdevelop-frontend && npm run lint
```

## Contributing:
Only branch from Develop. Once code is being pushed to master, it will be pushed to the amazon S3 bucket. Create a Merge Request on Gitlab.