import { createRouter, createWebHistory } from 'vue-router';
import homePage from '@/pages/homePage';
import { usePageInfoStore } from '@/stores/pageInfoStore';

const DEFAULT_TITLE = 'Erikdevelop.com';


const beforeEnter = (to, from, next) => {
  const pageInfoStore = usePageInfoStore();

  pageInfoStore.pageVisible = false;
  setTimeout(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
    pageInfoStore.pageStarted = false;
    next();
  }, 250);
}

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: "Home",
      component: homePage,
      meta: {
        title: 'Home - Erikdevelop.com'
      },
      beforeEnter: beforeEnter
    }
  ]
});

export default router