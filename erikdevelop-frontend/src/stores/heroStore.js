import { defineStore } from "pinia";
import { ref } from "vue";

export const useHeroStore = defineStore('hero', () => {
  const currentHero = ref('');

  return {
    currentHero
  }
});