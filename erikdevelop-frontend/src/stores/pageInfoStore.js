import { defineStore } from "pinia";
import { ref } from "vue";
import axios from 'axios';

export const usePageInfoStore = defineStore('pageInfo', () => {
  const pageInfo = ref({});
  const pageVisible = ref(false);
  const pageStarted = ref(false);

  async function getPageInfo() {
    if (Object.keys(pageInfo.value).length === 0 && pageInfo.value.constructor === Object) {
      const { data } = await axios.get('/static/data/pageInfo.json');

      pageInfo.value = data.data;
    }

    return pageInfo.value;
  }

  return {
    pageVisible,
    pageStarted,
    getPageInfo
  }
});