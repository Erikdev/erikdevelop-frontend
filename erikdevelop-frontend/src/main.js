import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from '@/App.vue';
import router from '@/router';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faHome, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import { faAws, faVuejs, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faHome);
library.add(faBookOpen);
library.add(faAws);
library.add(faVuejs);
library.add(faGitlab);

const pinia = createPinia();

const app = createApp(App);
app.use(pinia);
app.use(router);
app.component('icon', FontAwesomeIcon);
app.mount('#app');
